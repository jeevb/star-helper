import functools
import os

def is_valid_dir(path):
    return os.path.exists(path) and os.path.isdir(path)

def is_valid_file(path):
    return os.path.exists(path) and os.path.isfile(path)

def new_or_existing_dir(path, empty_check=False):
    if not is_valid_dir(path):
        os.makedirs(path)

    if empty_check and os.listdir(path):
        raise IOError(
            'Directory %r already exists and is not empty.' % args.output
        )

    return path

def verbose(msg):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
            except Exception, e:
                if not hasattr(e, 'trace'):
                    e.trace = []
                e.trace.append(msg)
                raise
            else:
                return result
        return wrapper
    return decorator
