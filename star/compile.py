import os
import re
import shutil

from collections import defaultdict

from .helpers import is_valid_dir, new_or_existing_dir, verbose

__VALID_FIELDS__ = ['sample', 'extension', 'read', 'part']
__REQUIRED_FIELDS__ = ['sample', 'extension']


class FileObject(object):
    def __init__(self, path, sample, extension, read=None, part=None):
        super(FileObject, self).__init__()
        self.path = path
        self.sample = sample
        self.extension = extension.lstrip('.')
        self.read = int(read) if read is not None else 1
        self.part = int(part) if part is not None else 1

    @property
    def dest_file(self):
        return '{sample}_R{read}.{extension}'.format(sample=self.sample,
                                                     read=self.read,
                                                     extension=self.extension)

    def __lt__(self, comp):
        return(
            self.sample < comp.sample or
            self.read < comp.read or
            self.part < comp.part
        )

    def __repr__(self):
        return self.path

@verbose('validating input pattern fields')
def chk_fields(fields):
    for field in __REQUIRED_FIELDS__:
        if field not in fields:
            raise RuntimeError('Input pattern must specify %r field.' % field)

    for field in fields:
        if field not in __VALID_FIELDS__:
            raise RuntimeError(
                'Invalid field %r specified in input pattern.' % field
            )

@verbose('retrieving input files')
def find_files(path, pattern):
    path = os.path.expanduser(path)
    if not is_valid_dir(path):
        raise IOError('Input is non-existent or an invalid directory.')

    _pattern = re.compile(pattern)

    dest_files = defaultdict(list)
    for root, _, files in os.walk(path):
        for f in files:
            match = _pattern.match(f)
            if match:
                chk_fields(match.groupdict().iterkeys())
                obj = FileObject(path=os.path.join(root, f),
                                 **match.groupdict())
                dest_files[obj.sample, obj.dest_file].append(obj)

    if not dest_files:
        raise IOError(
            'No files found matching pattern %r in input directory %r.' % (
                pattern,
                path
            )
        )

    return dest_files

@verbose('writing out formatted files')
def write_files(path, dest_files):
    path = os.path.expanduser(path)
    output_dir = new_or_existing_dir(path, empty_check=True)

    for (sample, dest_file), source_files in dest_files.iteritems():
        sample_dir = new_or_existing_dir(os.path.join(output_dir, sample))

        with open(os.path.join(sample_dir, dest_file), 'wb') as o:
            for obj in sorted(source_files):
                with open(obj.path, 'rb') as i:
                    shutil.copyfileobj(i, o)
