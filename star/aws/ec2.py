import boto3

from ..helpers import verbose


class EC2(object):
    __service__ = 'ec2'

    @verbose('initializing a session')
    def __init__(self, cfg):
        super(EC2, self).__init__()
        self._cfg = cfg

        self._session = boto3.session.Session(
            region_name=self._cfg.get('resource', 'region'),
            aws_access_key_id=self._cfg.get('credentials', 'access_key_id'),
            aws_secret_access_key=self._cfg.get('credentials',
                                                'secret_access_key')
        )

    @property
    def _client(self):
        return self._session.client(service_name=self.__service__)

    @property
    def _resource(self):
        return self._session.resource(service_name=self.__service__)

    @verbose('retrieving an image ID by name')
    def _get_image_id_by_name(self, image):
        response = self._client.describe_images(
            Filters=[{'Name': 'name', 'Values': [image]}]
        )

        images = response['Images']
        if not images:
            raise RuntimeError('Image %r does not exist.' % image)

        return images[0]['ImageId']

    @verbose('retrieving a list of active instances')
    def instances(self, instance_id=None):
        # Only show instances corresponding to the specified keypair
        filters = [
            {'Name': 'key-name', 'Values': [self._cfg.get('ec2', 'keypair')]}
        ]

        # If instance_id is specified, add that to the filter
        if instance_id is not None:
            to_describe = list(instance_id) \
                if hasattr(instance_id, '__iter__') else [instance_id]
            filters.append({'Name': 'instance-id', 'Values': to_describe})

        response = self._client.describe_instances(Filters=filters)
        for r in response['Reservations']:
            for i in r['Instances']:
                if i['State']['Name'] == 'terminated':
                    continue

                # IP address is not returned in the response if it is not
                # available
                ip = i['PublicIpAddress'] if 'PublicIpAddress' in i \
                    else None

                yield (
                    i['InstanceId'],
                    i['State']['Name'],
                    i['InstanceType'],
                    ip,
                    i['KeyName']
                )

    @verbose('launching a new instance')
    def launch(self):
        for instance, _, _, _, _ in self.instances():
            raise RuntimeError(
                'An active instance %r already exists.' % instance
            )

        return self._resource.create_instances(
            ImageId=self._get_image_id_by_name(self._cfg.get('ec2', 'image')),
            MinCount=1,
            MaxCount=1,
            KeyName=self._cfg.get('ec2', 'keypair'),
            SecurityGroupIds=self._cfg.get('ec2', 'security_groups',
                                           split=','),
            InstanceType=self._cfg.get('ec2', 'instance_type')
        )

    @verbose('terminating an instance')
    def terminate(self, instance_id=None):
        ids = [i for i, _, _, _, _ in self.instances(instance_id)]
        if ids:
            response = self._client.terminate_instances(InstanceIds=ids)
            for i in response['TerminatingInstances']:
                yield (
                    i['InstanceId'],
                    i['PreviousState']['Name'],
                    i['CurrentState']['Name']
                )
