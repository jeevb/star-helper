import ConfigParser
import os
import sys

from ..helpers import is_valid_file, new_or_existing_dir, verbose


class AWSConfig(ConfigParser.ConfigParser):
    @verbose('reading the configuration file')
    def read(self, path):
        if not is_valid_file(path):
            raise IOError('Configuration file %r not found.' % path)

        ConfigParser.ConfigParser.read(self, path)

    @verbose('retrieving a configuration option')
    def get(self, section, opt, split=None):
        val = ConfigParser.ConfigParser.get(self, section, opt)
        return val if split is None else val.split(split)

    @verbose('setting a configuration option')
    def set(self, section, opt, value, prompt=None):
        _prompt = prompt if prompt else opt.replace('_', ' ').capitalize()
        try:
            ConfigParser.ConfigParser.set(
                self,
                section,
                opt,
                raw_input('{0} [{1}]: '.format(_prompt, value)) or value
            )
        except KeyboardInterrupt:
            sys.exit()


@verbose('generating configuration template')
def generate_config_template(path):
    cfg = AWSConfig()

    cfg.add_section('credentials')
    cfg.set('credentials', 'access_key_id', '', 'AWS access key ID')
    cfg.set('credentials', 'secret_access_key', '', 'AWS secret access key')

    cfg.add_section('resource')
    cfg.set('resource', 'region', 'us-west-2')

    cfg.add_section('ec2')
    cfg.set('ec2', 'image', 'staR-pipeline', 'Image name')
    cfg.set('ec2', 'keypair', '')
    cfg.set('ec2', 'security_groups', '')
    cfg.set('ec2', 'instance_type', 'c3.8xlarge')

    with open(os.path.expanduser(path), 'wb') as o:
        cfg.write(o)

def read_config(path):
    cfg = AWSConfig()
    cfg.read(os.path.expanduser(path))

    return cfg
