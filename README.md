# staR-helper

A collection of helpers for [staR](https://bitbucket.org/jeevb/star), written in Python.

##### Setup:
```sh
# Clone the repository to your local
git clone https://bitbucket.org/jeevb/star-helper.git

# Change the directory to the cloned repository
cd star-helper

# Install the required python packages
pip install -r requirements.txt

# Add a symbolic link of staR-helper to your local PATH.
# ln -s /path/to/staR-helper /path/to/bin

# Initialize the configuration template for AWS actions.
staR-helper init
```

##### Examples:
```sh
# To launch an Amazon EC2 instance of the staR-pipeline.
staR-helper launch

# To show active instances.
staR-helper show

# To terminate active instances.
staR-helper terminate

# To compile raw reads files into a staR-friendly format.
staR-helper compile -p '(?P<sample>.+?)_R(?P<read>\d)_0+(?P<part>\d{1,3})\.(?P<extension>fastq\.gz)' -o reads -i temp
```
